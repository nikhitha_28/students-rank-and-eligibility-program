import java.util.*;

public class StudentsRank {
    static double averageMarks(HashMap<String, Double> marks) {
        double averageMarks = 0;
        for (Map.Entry mapElement : marks.entrySet()) {
            double value = (double) mapElement.getValue();
            averageMarks += value;
        }
        return averageMarks / 4;
    }
    public static void computerScienceEligibiltyChecker(HashMap<String, Double> marks) {
        String[] str = new String[2];
        for (Map.Entry mapElement : marks.entrySet()) {
            String subject = (String) mapElement.getKey();
            double mark = (double) mapElement.getValue();
            if (subject.equals("computer")) {
                if (mark > 80) {
                    str[0] = "yes";
                } else {
                    str[0] = "no";
                }
            }
        }
        Double physicsMarks = marks.get("physics");
        Double chemistryMarks = marks.get("chemistry");
        Double mathMarks = marks.get("math");
        double avgOfPcm =  (physicsMarks + chemistryMarks + mathMarks) / 3;
        if (avgOfPcm >= 70.5) {
            if (str[0].equals("yes")) {
                System.out.println("Student is eligible for Computer science group");
            } else {
                System.out.println("Student is not eligible for Computer science group");
            }
        }
    }
    public static void commerceEligibiltyChecker(HashMap<String, Double> marks) {
        for (Map.Entry mapElement : marks.entrySet()) {
            String subject = (String) mapElement.getKey();
            double mark = (double) mapElement.getValue();
            if (subject.equals("math")) {
                if (mark > 80) {
                    System.out.println("Student is eligible for Commerce group");
                } else {
                    System.out.println("Student is not eligible for Commerce group");
                }
            }
        }
    }

    public static void biologyEligibiltyChecker(HashMap<String, Double> marks) {
        Double physicsMarks = marks.get("physics");
        Double chemistryMarks = marks.get("chemistry");
        Double mathMarks = marks.get("math");
        double avgOfPcm =  (physicsMarks + chemistryMarks + mathMarks) / 3;
        if (avgOfPcm >= 70.5) {
            System.out.println("Student is eligible for Biology group");
        }
        else {
            System.out.println("Student is not eligible for Biology group");
        }
    }




    public static void main(String[] args) {
        HashMap<String, Double> marksList = new HashMap<>();
        Scanner scan = new Scanner(System.in);

        for (int i = 0; i < 4; i++) {
            String subjectName = scan.next();
            double individualMarks = scan.nextDouble();
            marksList.put(subjectName, individualMarks);
        }
        System.out.println(averageMarks(marksList));
        computerScienceEligibiltyChecker(marksList);
        commerceEligibiltyChecker(marksList);
        biologyEligibiltyChecker(marksList);
    }
}
